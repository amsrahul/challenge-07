// import { useState } from "react"
// import { useDispatch, useSelector } from "react-redux"
// import { setTitle } from "../reducers/ui-store"

import { useEffect } from "react"

const Home = () => {
    // const dispatch = useDispatch()
    // const currentTitle = useSelector(state => state.ui.title)
    // const [ customTitle, setCustomTitle ] = useState(currentTitle)
    useEffect(() => {
        if (window.loadOwlCarousel) {
            window.loadOwlCarousel()
        }
    }, [])
    return (
        <>
            <section className="our py-5" id="our">
        <div className="container">
            <div className="row p-lg-5 ">
                <div className="col-lg-6 col-md-12">
                    <img className="img-fluid service-img" src="/assets/img/our_service.svg" alt="service"/>
                </div>
                <div className="col-lg-6 col-md-12 my-auto">
                    <h3 className="font-weight-bold mb-4">Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                    <p className="text-justify">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah
                        dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan
                        wisata, bisnis, wedding, meeting, dll.</p>
                    <div className="d-flex mb-3">
                        <img className="pr-3" src="/assets/img/list.svg" alt="list"/>
                        <p className="my-auto">Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                    </div>
                    <div className="d-flex mb-3">
                        <img className="pr-3" src="/assets/img/list.svg" alt="list"/>
                        <p className="my-auto">Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                    </div>
                    <div className="d-flex mb-3">
                        <img className="pr-3" src="/assets/img/list.svg" alt="list"/>
                        <p className="my-auto">Sewa Mobil Jangka Panjang Bulanan</p>
                    </div>
                    <div className="d-flex mb-3">
                        <img className="pr-3" src="/assets/img/list.svg" alt="list"/>
                        <p className="my-auto">Gratis Antar - Jemput Mobil di Bandara</p>
                    </div>
                    <div className="d-flex mb-3">
                        <img className="pr-3" src="/assets/img/list.svg" alt="list"/>
                        <p className="my-auto">Layanan Airport Transfer / Drop In Out</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section className="why py-5" id="why">
        <div className="container">
            <h3>Why Us?</h3>
            <p>Mengapa harus pilih Binar Car Rental?</p>
            <div className="container row whyus isi">
                <div className="col-lg-3 col-md-12 card p-4">
                    <div className="mb-3">
                        <img src="/assets/img/jempol.svg" alt="icon"/>
                    </div>
                    <h5 className="mb-3">Mobil Lengkap</h5>
                    <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
                <div className="col-lg-3 col-md-12 card p-4">
                    <div className="mb-3">
                        <img src="/assets/img/harga.svg" alt="icon"/>
                    </div>
                    <h5 className="mb-3">Harga Murah</h5>
                    <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                </div>
                <div className="col-lg-3 col-md-12 card p-4">
                    <div className="mb-3">
                        <img src="/assets/img/jam.svg" alt="icon"/>
                    </div>
                    <h5 className="mb-3">Layanan 24 Jam</h5>
                    <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                </div>
                <div className="col-lg-3 col-md-12 card p-4">
                    <div className="mb-3">
                        <img src="/assets/img/supir.svg" alt="icon"/>
                    </div>
                    <h5 className="mb-3">Sopir Profesional</h5>
                    <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                </div>
            </div>
        </div>
    </section>

    <section className="testi py-5" id="test">
        <div className="overflow-hidden">
            <h3 className="text-center">Testimonial</h3>
            <p className="text-center">Berbagai review positif dari para pelanggan kami</p>

            <div id="owl-container">
                <div className="owl-carousel owl-theme">
                    <div className="item">
                        <div className="d-flex p-4">
                            <div className="mr-3 my-auto">
                                <img className="img-carousel" src="/assets/img/carousel/cowo.svg" alt="orang"/>
                            </div>
                            <div className="mb-2">
                                <p>⭐⭐⭐⭐⭐</p>
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                                </p>
                                <p className="font-weight-bold">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="d-flex p-4">
                            <div className="mr-3 my-auto">
                                <img className="img-carousel" src="/assets/img/carousel/cowo.svg" alt="orang"/>
                            </div>
                            <div className="mb-2">
                                <p>⭐⭐⭐⭐⭐</p>
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                                </p>
                                <p className="font-weight-bold">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="d-flex p-4">
                            <div className="mr-3 my-auto">
                                <img className="img-carousel" src="/assets/img/carousel/cowo.svg" alt="orang"/>
                            </div>
                            <div className="mb-2">
                                <p>⭐⭐⭐⭐⭐</p>
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                                </p>
                                <p className="font-weight-bold">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="d-flex p-4">
                            <div className="mr-3 my-auto">
                                <img className="img-carousel" src="/assets/img/carousel/cowo.svg" alt="orang"/>
                            </div>
                            <div className="mb-2">
                                <p>⭐⭐⭐⭐⭐</p>
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                                </p>
                                <p className="font-weight-bold">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section className="sewa py-5">
        <div className="container">
            <div className="deskripsi text-center">
                <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua. </p>
                <div className="btn btn-success mt-5 font-weight-bold">Mulai Sewa Mobil</div>
            </div>
        </div>
    </section>

    <section className="faq py-5" id="faq">
        <div className="container">
            <div className="row">
                <div className="col-lg-5 col-md-12">
                    <h3>Frequently Asked Question</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>

                <div className="col-lg-7 col-md-12">
                    <div className="accordion" id="accordionExample">
                        <div className="card"
                            style={{marginBottom: 16, borderBottom: "1px solid rgba(0,0,0,.125)", borderRadius: "0.25rem"}}>
                            <div className="card-header bg-white" id="headingOne" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <div className="d-flex justify-content-between">
                                    <p className="mb-0">Apa saja syarat yang dibutuhkan?</p>
                                    <img src="/assets/img/logoDown.svg" alt="down"/>
                                </div>
                            </div>
                            <div id="collapseOne" className="collapse" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div className="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                    Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                    you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div className="card"
                            style={{marginBottom: 16, borderBottom: "1px solid rgba(0,0,0,.125)", borderRadius: "0.25rem"}}>
                            <div className="card-header bg-white" id="headingTwo" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                <div className="d-flex justify-content-between">
                                    <p className="mb-0">Berapa hari minimal sewa mobil lepas kunci?</p>
                                    <img src="/assets/img/logoDown.svg" alt="down"/>
                                </div>
                            </div>
                            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div className="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                    Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                    you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div className="card"
                            style={{marginBottom: 16, borderBottom: "1px solid rgba(0,0,0,.125)", borderRadius: "0.25rem"}}>
                            <div className="card-header bg-white" id="headingThree" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                <div className="d-flex justify-content-between">
                                    <p className="mb-0">Berapa hari sebelumnya sabaiknya booking sewa mobil?</p>
                                    <img src="/assets/img/logoDown.svg" alt="down"/>
                                </div>
                            </div>
                            <div id="collapseThree" className="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div className="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                    Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                    you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div className="card"
                            style={{marginBottom: 16, borderBottom: "1px solid rgba(0,0,0,.125)", borderRadius: "0.25rem"}}>
                            <div className="card-header bg-white" id="headingFour" type="button" data-toggle="collapse"
                                data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                <div className="d-flex justify-content-between">
                                    <p className="mb-0">Apakah Ada biaya antar-jemput?</p>
                                    <img src="/assets/img/logoDown.svg" alt="down"/>
                                </div>
                            </div>
                            <div id="collapseFour" className="collapse" aria-labelledby="headingFour"
                                data-parent="#accordionExample">
                                <div className="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                    Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                    you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>

                        <div className="card"
                            style={{marginBottom: 16, borderBottom: "1px solid rgba(0,0,0,.125)", borderRadius: "0.25rem"}}
                            >
                            <div className="card-header bg-white" id="headingFive" type="button" data-toggle="collapse"
                                data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                <div className="d-flex justify-content-between">
                                    <p className="mb-0">Bagaimana jika terjadi kecelakaan</p>
                                    <img src="/assets/img/logoDown.svg" alt="down"/>
                                </div>
                            </div>
                            <div id="collapseFive" className="collapse" aria-labelledby="headingFive"
                                data-parent="#accordionExample">
                                <div className="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                                    sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                    shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                                    Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                    you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        </>
    )
}

export default Home